# Auto scaling
**Load balancer**
![Scheme](load-balancer.png)

**Target group**
![Scheme](target-group.png)

**Auto scaling**
![Scheme](auto-scaling-common.png)

![Scheme](auto-scaling-cpu.png)

![Scheme](auto-scaling-request.png)
